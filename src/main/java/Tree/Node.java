package Tree;

import java.util.*;

public class Node<K, V> {
  Optional<Node<K, V>> left = Optional.empty();
  Optional<Node<K, V>> right = Optional.empty();
  final int id;
  List<Node.Entry<K, V>> entries = new LinkedList<>();

  public Node(final int id) {
    this.id = id;
  }

  public int size() {
    int leftSize = 0;
    int rightSize = 0;

    if (left.isPresent()) {
      leftSize = left.get().size();
    }
    if (right.isPresent()) {
      rightSize = right.get().size();
    }
    return entries.size() + leftSize + rightSize;
  }

  public V put(final K key, final V value) {
    V prevVal = null;
    final int idKey = key.hashCode();
    if (id == idKey) {
      final Iterator<Node.Entry<K, V>> it = entries.iterator();
      while (it.hasNext()) {
        final Node.Entry<K, V> e = it.next();
        if (e.key.equals(key)) {
          prevVal = e.value;
          it.remove();
        }
      }
      entries.add(new Node.Entry<>(key, value));
    } else if (idKey < id) {
      if (left.isPresent()) {
        prevVal = left.get().put(key, value);
      } else {
        left = createNode(key, value);
        prevVal = null;
      }
    } else if (idKey > id) {
      if (right.isPresent()) {
        prevVal = right.get().put(key, value);
      } else {
        right = createNode(key, value);
        prevVal = null;
      }
    }
    return prevVal;
  }

  public V get(final Object key) {
    final int idKey = key.hashCode();
    if (id == idKey) {
      final Iterator<Node.Entry<K, V>> it = entries.iterator();
      while (it.hasNext()) {
        final Node.Entry<K, V> e = it.next();
        if (e.key.equals(key)) {
          return e.value;
        }
      }
      return null;
    } else if (idKey < id) {
      if (left.isPresent()) {
        return left.get().get(key);
      } else {
        return null;
      }
    } else {
      if (right.isPresent()) {
        return right.get().get(key);
      } else {
        return null;
      }
    }
  }

  public V remove(final Object key) {
    final int keyId = key.hashCode();
    if (keyId == id) {
      final Iterator<Node.Entry<K, V>> it = entries.iterator();
      while (it.hasNext()) {
        final Node.Entry<K, V> e = it.next();
        if (e.key.equals(key)) {
          it.remove();
          return e.value;
        }
      }
      return null;
    } else if (keyId < id) {
      if (left.isPresent()) {
        return left.get().remove(key);
      } else {
        return null;
      }
    } else {
      if (right.isPresent()) {
        return right.get().remove(key);
      } else {
        return null;
      }
    }
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    for (final Node.Entry<K, V> e : entries) {
      sb.append(e).append(' ');
    }
    return (left.isPresent() ? "Left: " + left.get().toString() : "")
        + sb.toString()
        + (right.isPresent() ? "Right: " + right.get().toString() : "");
  }

  public static <K, V> Optional<Node<K, V>> createNode(final K key, final V value) {
    final Node<K, V> node = new Node<>(key.hashCode());
    node.entries.add(new Node.Entry<>(key, value));
    return Optional.of(node);
  }

  public void addAllEntries(final Collection<Map.Entry<K, V>> s) {
    s.addAll(entries);
    if (left.isPresent()) {
      left.get().addAllEntries(s);
    }
    if (right.isPresent()) {
      right.get().addAllEntries(s);
    }
  }

  private static class Entry<K, V> implements Map.Entry<K, V> {
    private final K key;
    private V value;

    public Entry(final K key, final V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public String toString() {
      return key + "=>" + value;
    }

    @Override
    public K getKey() {
      return key;
    }

    @Override
    public V getValue() {
      return value;
    }

    @Override
    public V setValue(final V value) {
      final V old = this.value;
      this.value = value;
      return old;
    }
  }
}
